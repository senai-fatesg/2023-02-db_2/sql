## Índices

Primeiramente busque, através do sql abaixo, a atual performance da instrução

```sql
explain analyse
select * from candidato c 
join boletim_urna bu on bu.nr_cpf_candidato = c.nr_cpf_candidato 
where bu.nr_cpf_candidato = '01099561760'

```

Verifique as instruções *EXPLAIN ANALYSE* antes do sql.

Note a resposta, ainda sem indice:

```
Nested Loop  (cost=1000.00..355838.21 rows=13080 width=1033) (actual time=4.040..3848.680 rows=14587 loops=1)
  ->  Seq Scan on candidato c  (cost=0.00..2819.43 rows=1 width=613) (actual time=3.439..8.593 rows=1 loops=1)
        Filter: ((nr_cpf_candidato)::text = '01099561760'::text)
        Rows Removed by Filter: 29313
  ->  Gather  (cost=1000.00..352887.99 rows=13080 width=420) (actual time=0.591..3832.176 rows=14587 loops=1)
        Workers Planned: 2
        Workers Launched: 2
        ->  Parallel Seq Scan on boletim_urna bu  (cost=0.00..350579.99 rows=5450 width=420) (actual time=681.540..3775.966 rows=4862 loops=3)
              Filter: ((nr_cpf_candidato)::text = '01099561760'::text)
              Rows Removed by Filter: 725889
Planning Time: 4.421 ms
Execution Time: 3849.097 ms
```

Você pode colocar esta resposta no site: https://explain.depesz.com/

### Criando os índices

```sql
CREATE INDEX idx_boletim_urna_cpf ON boletim_urna(nr_cpf_candidato);
CREATE INDEX idx_candidato_cpf ON candidato(nr_cpf_candidato);

```

Reexecute o explain acima. Observe a melhoria no resultado:

```
Nested Loop  (cost=150.09..44295.64 rows=13080 width=1033) (actual time=1.273..15.615 rows=14587 loops=1)
  ->  Index Scan using idx_candidato_cpf on candidato c  (cost=0.29..8.30 rows=1 width=613) (actual time=0.024..0.027 rows=1 loops=1)
        Index Cond: ((nr_cpf_candidato)::text = '01099561760'::text)
  ->  Bitmap Heap Scan on boletim_urna bu  (cost=149.80..44156.54 rows=13080 width=420) (actual time=1.241..5.857 rows=14587 loops=1)
        Recheck Cond: ((nr_cpf_candidato)::text = '01099561760'::text)
        Heap Blocks: exact=4476
        ->  Bitmap Index Scan on idx_boletim_urna_cpf  (cost=0.00..146.53 rows=13080 width=0) (actual time=0.762..0.762 rows=14587 loops=1)
              Index Cond: ((nr_cpf_candidato)::text = '01099561760'::text)
Planning Time: 2.089 ms
Execution Time: 15.975 ms
```