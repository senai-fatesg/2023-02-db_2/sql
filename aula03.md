# Select com JOIN

Primeiro, foi importado os dados de candidato do TSE

https://dadosabertos.tse.jus.br/dataset/candidatos-2022

```sql
select c.nr_cpf_candidato, c.nm_candidato, c.nm_urna_candidato,
c.cd_eleicao, bu.cd_tipo_eleicao, c.nr_candidato, bu.nr_votavel, c.sg_uf, bu.nm_votavel, * 
from candidato c 
join boletim_urna bu on c.cd_eleicao = bu.cd_eleicao 
and c.nr_candidato = bu.nr_votavel 
and c.sg_uf = bu.sg_uf 
and upper(c.ds_cargo) = upper(bu.ds_cargo_pergunta)

```

```sql
select c.ds_genero, sum(bu.qt_votos::integer)  
from candidato c 
join boletim_urna bu on c.cd_eleicao = bu.cd_eleicao 
and c.nr_candidato = bu.nr_votavel 
and c.sg_uf = bu.sg_uf 
and upper(c.ds_cargo) = upper(bu.ds_cargo_pergunta)
group by c.ds_genero 

```

```sql
select c.ds_grau_instrucao, sum(bu.qt_votos::integer)  
from candidato c 
join boletim_urna bu on c.cd_eleicao = bu.cd_eleicao 
and c.nr_candidato = bu.nr_votavel 
and c.sg_uf = bu.sg_uf 
and upper(c.ds_cargo) = upper(bu.ds_cargo_pergunta)
group by c.ds_grau_instrucao  

```

```sql
select ds_sit_tot_turno, ds_grau_instrucao, count(*) as QTD   from candidato c 
where c.sg_uf = 'GO' and ds_sit_tot_turno like 'ELEITO%' and ds_cargo not like '%SUPLENTE%' 
group by ds_sit_tot_turno, ds_grau_instrucao 
order by qtd

```

```sql
select c.ds_grau_instrucao, ds_sit_tot_turno, sum(bu.qt_votos::integer) as qtd
from candidato c 
join boletim_urna bu on c.cd_eleicao = bu.cd_eleicao 
and c.nr_candidato = bu.nr_votavel 
and c.sg_uf = bu.sg_uf 
and upper(c.ds_cargo) = upper(bu.ds_cargo_pergunta)
where ds_sit_tot_turno like 'ELEITO%'
group by ds_sit_tot_turno, c.ds_grau_instrucao  
order by qtd
```
