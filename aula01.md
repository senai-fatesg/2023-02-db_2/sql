```

-- lista todos os registros
select * from boletim_urna bu 

--lista a qtd de votos de um candidato
select sum(bu.qt_votos::integer) from boletim_urna bu where bu.nm_votavel 
= 'GUSTAVO GAYER'

--lista a qtd de votos de um candidato
select sum(bu.qt_votos::integer) from boletim_urna bu where lower(bu.nm_votavel)
= 'gustavo gayer'

select sum(bu.qt_votos::integer) from boletim_urna bu where lower(bu.nm_votavel)
= 'nulo'

select count(*) from boletim_urna bu 

--lista brancos e nulos
select ds_cargo_pergunta, sum(bu.qt_votos::integer) from boletim_urna bu where lower(bu.nm_votavel)
in ('nulo', 'branco')
group by ds_cargo_pergunta

-- lista brancos e nulos por municipio
select nm_municipio, ds_cargo_pergunta, sum(bu.qt_votos::integer) from boletim_urna bu where lower(bu.nm_votavel)
in ('nulo', 'branco')
group by ds_cargo_pergunta, nm_municipio order by ds_cargo_pergunta, nm_municipio 

select * from boletim_urna bu 
```
