## SQLs ordinários (que você tem que aprender)

```
-- lista todos os registros
select * from boletim_urna
```


```
-- define as colunas que você deseja retornar
select nr_partido, nm_votavel, cd_municipio, nm_municipio, nr_zona, nr_secao, nr_local_votacao, ds_cargo_pergunta
from boletim_urna
```


```
-- exemplo da utilização de where
select nr_partido, nm_votavel, cd_municipio, nm_municipio, nr_zona, nr_secao, nr_local_votacao, ds_cargo_pergunta
from boletim_urna where nm_votavel = 'DANIEL AGROBOM' and 'DR. DIONI' = nm_votavel or 1 = 1 
```


```
-- exemplo da utilização de order by
select nm_votavel as nome, qt_votos as quantidade from boletim_urna bu where nr_zona = '127' and nr_secao = '112' order by nm_votavel
```





## SQLs extaordinários, que utilizamos pare resolver questões difícies.


```
-- lista os dois maiores presidenciaveis com sua respectiva qtd de votos
select 'LULA', SUM(qt_aptos::INTEGER), SUM(qt_comparecimento::INTEGER), SUM(qt_votos::INTEGER) from boletim_urna where nm_votavel = 'LULA'
union 
select 'BOLSONARO', SUM(qt_aptos::INTEGER), SUM(qt_comparecimento::INTEGER), SUM(qt_votos::INTEGER) from boletim_urna where nm_votavel = 'JAIR BOLSONARO'
```


```
select nm_votavel, qt_votos, * from boletim_urna where nr_zona = '127' and nr_secao = '112' order by nm_votavel
```


```
select count(*) from
(select distinct nr_urna_efetivada  from boletim_urna bu) tbl 
```

14620

```
select max(qt_votos::integer) from boletim_urna bu
select * from boletim_urna bu where qt_votos::integer = (select max(qt_votos::integer) from boletim_urna bu)
```

```
select * from
(select nr_urna_efetivada, nm_municipio, sum(qt_votos::integer) as qtd  from boletim_urna group by nr_urna_efetivada, nm_municipio) tbl
order by tbl.qtd asc
```


```
select * from boletim_urna bu 
```

## Exercicio da aula

Listar os candidatos que receberam votação na sua seção eleitoral

## Desafio da aula

1. Liste os municipios que tiveram votação
2. Liste a urna que recebeu menos votos e informe a qtd de votos recebidos
3. Liste o candidato que recebeu menos votos e informe q qtd de votos recebidos