## Ajustes dos dados

```sql
-- ajusta o cpf dos demais candidatos
update
	boletim_urna as bu set
	nr_cpf_candidato = c.nr_cpf_candidato
from
	candidato c
where 
	c.cd_eleicao = bu.cd_eleicao
	and c.nr_candidato = bu.nr_votavel
	and c.sg_uf = bu.sg_uf
	and upper(c.ds_cargo) = upper(bu.ds_cargo_pergunta);
```

```sql
-- ajusta os cpfs dos presidentes
update
	boletim_urna as bu set
	nr_cpf_candidato = c.nr_cpf_candidato
from
	candidato c
where
	bu.nr_cpf_candidato is null
	and c.nm_urna_candidato = bu.nm_votavel
	and c.sg_uf = 'BR';
```

```sql
-- ajusta o cpf do presidenciavel FELIPE D AVILA (problema do apóstrofo)
update
	boletim_urna
set
	nr_cpf_candidato = '08720335890'
where
	nr_votavel = '30'
	and nm_votavel = 'FELIPE D''AVILA'
	and ds_cargo_pergunta = 'Presidente'
```

```sql
-- ajusta os tipos das colunas numericas	
ALTER TABLE boletim_urna  ALTER COLUMN qt_votos TYPE integer USING (qt_votos::integer);
ALTER TABLE boletim_urna  ALTER COLUMN qt_abstencoes TYPE integer USING (qt_abstencoes::integer);
ALTER TABLE boletim_urna  ALTER COLUMN qt_comparecimento TYPE integer USING (qt_comparecimento::integer);
ALTER TABLE boletim_urna  ALTER COLUMN qt_aptos TYPE integer USING (qt_aptos::integer);
```sql	
	

## Exemplos de JOIN

```sql
select
	*
from
	boletim_urna bu
join candidato c on
	c.nr_cpf_candidato = bu.nr_cpf_candidato
where
	nm_votavel = 'Branco'
```

```sql
select
	*
from
	boletim_urna bu
left join candidato c on
	c.nr_cpf_candidato = bu.nr_cpf_candidato
where
	nm_votavel = 'Branco'
```
	
```sql	
select
	*
from
	boletim_urna bu
right join candidato c on
	c.nr_cpf_candidato = bu.nr_cpf_candidato
where
	c.nm_urna_candidato = 'JULIO KULLER'
```