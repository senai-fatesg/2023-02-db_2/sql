## Criação de Chave primaria

### Primeiro, descobrindo se o nr_cpf_candidato possui valores repetidos

```
select nr_cpf_candidato, count(*) as qtd from candidato c group by nr_cpf_candidato 
having count(*) > 1
```

Após a execução, conclui-se que tem.

### Criação do campo id, para funcionar como chave

```
alter table candidato add column id serial
```

### Definição do campo id como chave primária

```
ALTER TABLE public.candidato ADD CONSTRAINT candidato_pk PRIMARY KEY (id);
```

### Teste da chave primaria

```
insert into candidato (nr_cpf_candidato, nm_candidato)
values ('93619967114', 'Francisco Calaca Xavier')

select * from candidato c order by id desc
delete from candidato c where id = 29315
```

## Criação de Chave estraneira

### Primeiro, criar a coluna, em boletim_urna que será a ligação com candidato


```
alter table boletim_urna add column candidato_id integer
```

### Depois, popular esta coluna

```
update boletim_urna as bu set candidato_id = c.id from candidato c 
where bu.nr_cpf_candidato = c.nr_cpf_candidato and c.st_candidato_inserido_urna = 'SIM'
```

### Apesar disso, ainda ficaram alguns cpfs sem id, tente explicar e corrigir

```
select distinct nr_cpf_candidato  from boletim_urna bu where bu.candidato_id is null and nr_cpf_candidato is not null
```

```
select * from candidato c where nr_cpf_candidato = '41054270163'
```

### Finalmente, criação da foreign key

```
ALTER TABLE public.boletim_urna ADD CONSTRAINT boletim_urna_fk FOREIGN KEY (candidato_id) REFERENCES public.candidato(id);
```

### Teste da foreign key

```
insert into boletim_urna (qt_votos, candidato_id) values (10, 1) --deu certo
select * from boletim_urna where candidato_id = 1 and qt_votos = 10 -- deu certo

insert into boletim_urna (qt_votos, candidato_id) values (10, 50000) --nao deu certo
```
